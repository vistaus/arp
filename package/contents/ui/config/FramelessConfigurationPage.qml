/*
 *  SPDX-FileCopyrightText: 2023 ivan tkachenko <me@ratijas.tk>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQml 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.20 as Kirigami

ColumnLayout {
    id: root

    Layout.fillWidth: true
    Layout.fillHeight: true

    spacing: 0

    // Try to remove extra padding enforced by ConfigurationAppletPage. If it
    // succeeds, we'll have slightly better UI. If it fails, it's OK too.
    Binding {
        target: {
            let parent = root.parent;
            while (parent !== null && !(parent instanceof Kirigami.Page)) {
                parent = parent.parent;
            }
            return parent;
        }
        restoreMode: Binding.RestoreBindingOrValue
        property: 'padding'
        value: 0
    }
}
