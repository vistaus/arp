/*
 *  SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtMultimedia 5.15
import org.kde.plasma.core 2.1 as PlasmaCore
import org.kde.plasma.plasmoid 2.0

Item {
    anchors.fill: parent

    PlasmaCore.IconItem {
        source: Plasmoid.configuration.icon
        anchors.fill: parent
        overlays: isPlaying() && playMusic.status === MediaPlayer.Buffered
            ? setIndicator(Plasmoid.configuration.indicator)
            : []
    }

    function setIndicator(icon) {
        const overlay = ["", "", "", ""]
        overlay[Plasmoid.configuration.indicatorIndex] = icon
        return overlay
    }
}
