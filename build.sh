#!/bin/sh
# Version: 6

# SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
# SPDX-FileCopyrightText: 2023 ivan tkachenko <me@ratijas.tk>
#
# SPDX-License-Identifier: LGPL-2.0-or-later

# This script will convert the *.po files to *.mo files, rebuilding the package/contents/locale folder.
# Feature discussion: https://phabricator.kde.org/D5209
# Eg: package/contents/locale/fr_CA/LC_MESSAGES/plasma_applet_org.kde.plasma.eventcalendar.mo

cd "$( dirname "${BASH_SOURCE[0]}" )"

PROJECT=org.kde.plasma.advancedradio
TRANSLATION_DOMAIN="plasma_applet_$PROJECT"
podir=translations

echo "[build] Compiling message catalogs..."

typeset catalogs=`find $podir -name '*.po' | sort`
for catalog in $catalogs; do
	locale=${catalog%/*}  # strip file name
	locale=${locale##*/}  # and parent directories

	install_dir="package/contents/locale/${locale}/LC_MESSAGES"
	install_path="$install_dir/${TRANSLATION_DOMAIN}.mo"

	echo "[build] Installing message catalogs for $locale -> $install_path"

	mkdir -p "$install_dir"
	msgfmt -o "${install_path}" "$catalog"
done

echo "[build] Compiling message catalogs: done"

if [ "$1" = "--restartplasma" ]; then
	echo "[build] Restarting plasmashell..."
	kquitapp5 --wait plasmashell
	kstart5 plasmashell
	echo "[build] Restarting plasmashell: done"
else
	echo "[build] (re)install the plasmoid and restart plasmashell, or run \`plasmoidviewer -a $PROJECT\` to test."
fi
